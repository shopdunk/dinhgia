const phonelist = {
  "iPhone 5": {
    color: [["xam", "Xám"], ["bac", "Bạc"], ["vang", "Vàng"]],
    gb: ["16GB", "32GB", "64GB"]
  },

  "iPhone 5s": {
    color: [["xam", "Xám"], ["bac", "Bạc"], ["vang", "Vàng"]],
    gb: ["16GB", "32GB", "64GB"]
  },
  "iPhone SE": {
    color: [["xam", "Xám"], ["bac", "Bạc"], ["vang", "Vàng"], ["hong", "Hồng"]],
    gb: ["16GB", "32GB", "64GB"]
  },

  "iPhone 6": {
    color: [["xam", "Xám"], ["bac", "Bạc"], ["vang", "Vàng"]],
    gb: ["16GB", "32GB", "64GB"]
  },

  "iPhone 6 Plus": {
    color: [["xam", "Xám"], ["bac", "Bạc"], ["vang", "Vàng"]],
    gb: ["16GB", "32GB", "64GB"]
  },

  "iPhone 6s": {
    color: [["xam", "Xám"], ["bac", "Bạc"], ["vang", "Vàng"], ["hong", "Hồng"]],
    gb: ["16GB", "32GB", "64GB"]
  },

  "iPhone 6s Plus": {
    color: [["xam", "Xám"], ["bac", "Bạc"], ["vang", "Vàng"], ["hong", "Hồng"]],
    gb: ["16GB", "32GB", "64GB"]
  },

  "iPhone 7": {
    color: [
      ["xam", "Xám"],
      ["bac", "Bạc"],
      ["vang", "Vàng"],
      ["hong", "Hồng"],
      ["do", "Đỏ"],
      ["denbong", "Đen Bóng"]
    ],
    gb: ["32GB", "128GB", "256GB"]
  },

  "iPhone 7 Plus": {
    color: [
      ["xam", "Xám"],
      ["bac", "Bạc"],
      ["vang", "Vàng"],
      ["hong", "Hồng"],
      ["do", "Đỏ"],
      ["denbong", "Đen Bóng"]
    ],
    gb: ["32GB", "128GB", "256GB"]
  },

  "iPhone 8": {
    color: [["xam", "Xám"], ["bac", "Bạc"], ["vang", "Vàng"], ["do", "Đỏ"]],
    gb: ["64GB", "256GB"]
  },

  "iPhone 8 Plus": {
    color: [["xam", "Xám"], ["bac", "Bạc"], ["vang", "Vàng"], ["do", "Đỏ"]],
    gb: ["64GB", "256GB"]
  },

  "iPhone X": {
    color: [["xam", "Xám"], ["trang", "Trắng"]],
    gb: ["64GB", "256GB"]
  },

  "iPhone XS": {
    color: [["den", "Đen"], ["bac", "Bạc"], ["vang", "Vàng"]],
    gb: ["64GB", "256GB", "512GB"]
  },

  "iPhone XS Max": {
    color: [["den", "Đen"], ["bac", "Bạc"], ["vang", "Vàng"]],
    gb: ["64GB", "256GB", "512GB"]
  },

  "iPhone XR": {
    color: [
      ["xam", "Xám"],
      ["trang", "Trắng"],
      ["vang", "Vàng"],
      ["xanhduong", "Xanh Dương"],
      ["cam", "Cam"],
      ["do", "Đỏ"]
    ],
    gb: ["64GB", "128GB", "256GB"]
  },

  "iPhone 11": {
    color: [
      ["den", "Đen"],
      ["trang", "Trắng"],
      ["vang", "Vàng"],
      ["xanhbacha", "Xanh Bạc Hà"],
      ["do", "Đỏ"],
      ["tim", "Tím"]
    ],
    gb: ["64GB", "128GB", "256GB"]
  },

  "iPhone 11 Pro": {
    color: [
      ["xam", "Xám"],
      ["trang", "Trắng"],
      ["vang", "Vàng"],
      ["xanhreu", "Xanh Rêu"]
    ],
    gb: ["64GB", "256GB", "512GB"]
  },

  "iPhone 11 Pro Max": {
    color: [
      ["xam", "Xám"],
      ["trang", "Trắng"],
      ["vang", "Vàng"],
      ["xanhreu", "Xanh Rêu"]
    ],
    gb: ["64GB", "256GB", "512GB"]
  }
};
