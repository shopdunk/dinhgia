// class Evaluation session has an id
class Evaluation {
  constructor(
    sessionID,
    sku,
    gb,
    color,
    baseprice,
    result,
    physical,
    warranty,
    box,
    vn,
    fromshopdunk,
    willshopdunk
  ) {
    (this.sessionID = sessionID),
      (this.sku = sku),
      (this.gb = gb),
      (this.color = color),
      this.baseprice,
      (this.physical = physical),
      (this.warranty = warranty),
      (this.box = box),
      (this.vn = vn),
      (this.fromshopdunk = fromshopdunk),
      (this.willshopdunk = willshopdunk),
      this.result,
      this.message,
      this.name,
      this.phonenum;
  }
  // take data from a radio group and assign it to equivalent property of object anEvaluation
  fromRadioToObjValue(cssclass) {
    try {
      this[bemToVar(cssclass)] = [
        ...document.getElementsByClassName(cssclass)
      ].find(radio => radio.checked).value;
    } catch (err) {
      this[bemToVar(cssclass)] = undefined;
    }
  }
  // take data from a dropdown (select option )and assign it to equivalent property of object anEvaluation
  fromDropdownToObjValue(cssid) {
    try {
      this[bemToVar(cssid)] = document.getElementById(cssid).value;
    } catch (err) {
      this[bemToVar(cssid)] = undefined;
    }
  }

  // check if user choose phone sku, memory and color
  checkPhoneFields() {
    return (
      this.sku !== "defaultsku" &&
      this.gb !== undefined &&
      this.color !== undefined
    );
  }
  // when the user click the "evaluate the price" button
  evaluatePrice() {
    // update base price
    this.baseprice = parseInt(
      document.getElementById("eval__baseprice").value.replace(/\D/g, "")
    );

    // sku
    idOfFields.map(cssid => {
      this.fromDropdownToObjValue(cssid);
    });

    // color, gb
    classNamesOfNonCalFields.map(cssclass => {
      this.fromRadioToObjValue(cssclass);
    });

    // warranty, vn, box, willsopdunk, fromshopdunk, physical
    classNamesOfFields.map(cssclass => {
      this.fromRadioToObjValue(cssclass);
    });

    // tell customer to fill in all the fields if something is not filled

    if (!checkInputPrice(this.baseprice) && this.checkPhoneFields()) {
      alert("Vui lòng nhập đúng giá");
    } else if (checkInputPrice(this.baseprice) && !this.checkPhoneFields()) {
      alert("Vui lòng chọn đủ thông tin điện thoại");
    } else if (!checkInputPrice(this.baseprice) && !this.checkPhoneFields()) {
      alert("Vui lòng nhập đúng giá và chọn đủ thông tin điện thoại");
    } else {
      this.sessionID = Math.floor(Math.random() * 1000000);
      // really calculate the result

      this.result =
        (this.baseprice / 100) *
        (companyRate.physical[this.physical].rate +
          companyRate.warranty[this.warranty].rate +
          companyRate.box[this.box].rate +
          companyRate.vn[this.vn].rate +
          companyRate.willshopdunk[this.willshopdunk].rate +
          companyRate.fromshopdunk[this.fromshopdunk].rate);

      // location.href = "./pages/customerscontact.html";
      // document.getElementById("eval__page-1").style.display = "none";
      // document.getElementById("eval__page-3").style.display = "none";
      // document.getElementById("eval__page-2").style.display = "block";
      showWhichPage("eval__page-2");
    }
  }
}
// WHEN CUSTOMER LOAD THE PAGE OR CLICK SUBMIT THE FORM Create new Customer instance when someone load the page

let anEvaluation = new Evaluation();

// object that hols the percentage deduction from guarant value (business logic)

const criteria2 = {
  physical: {
    a: { rate: 82, message: "Loại A" },
    b: { rate: 79.5, message: "Loại B" },
    c: { rate: 77, message: "Loại C" },
    notsure: { rate: 77, message: "Không rõ loại" }
  },
  warranty: {
    no: { rate: 0, message: "đã hết bảo hành" },
    to3: { rate: 0.5, message: "còn 1-3 tháng bảo hành" },
    to6: { rate: 1, message: "còn 4-6 tháng bảo hành" },
    to9: { rate: 1.5, message: "còn 7-9 tháng bảo hành" },
    to12: { rate: 2, message: "còn 9-12 tháng bảo hành" },
    notsure: { rate: 0, message: "không rõ về thời hạn bảo hành còn lại" }
  },
  box: {
    yes: { rate: 0.5, message: "có kèm hộp" },
    no: { rate: 0, message: "không còn hộp" },
    notsure: { rate: 0, message: "không rõ tình trạng hộp" }
  },
  vn: {
    yes: { rate: 1, message: "phiên bản Việt Nam" },
    no: { rate: 0, message: "phiên bản quốc tế" },
    notsure: { rate: 0, message: "không rõ phiên bản" }
  },
  fromshopdunk: {
    yes: { rate: 1, message: "mua tại ShopDunk" },
    no: { rate: 0, message: "mua tại cửa hàng khác ShopDunk" },
    notsure: { rate: 0, message: "không rõ nơi mua" }
  },
  willshopdunk: {
    yes: { rate: 1, message: "bạn sẽ tiếp tục mua hàng tại ShopDunk" },
    no: { rate: 0, message: "bạn sẽ không mua tiếp ở ShopDunk" },
    notsure: { rate: 0, message: "bạn không rõ nơi mua máy lên đời" }
  }
};
let companyRate = criteria2;

//  print to the color and gb fields (wil trigger when eval__sku onclick or the content of eval__sku onchange)
function printColorandGb() {
  let colors = phonelist[evalsku.value].color; // all colors of the phone sku customer chose
  let gbs = phonelist[evalsku.value].gb;

  let tempGbNode = ``;
  let tempColorNode = ``;
  gbs.map(
    (gb, i) =>
      (tempGbNode += `<li class="eval__radio-n-label">
   <input
     type="radio"
     class="eval__radio eval__gb"
     id="eval__gb1${i}"
     name="eval__gb"
     value="${gb}"
   /><label for="eval__gb${i}" class="eval__label"> ${gb}</label>
   </li>`)
  );
  colors.map(
    (color, i) =>
      (tempColorNode += `<li class="eval__radio-n-label-color">
<input
  type="radio"
  class="eval__radio eval__color"
  id="eval__color-${color[0]}"
  name="eval__color"
  value="${color[1]}"
/><label
  for="eval__color-${color[0]}"
  class="eval__label-color eval__label"
></label>
</li>`)
  );

  // innerHTML to the choose phone fields
  gbNode.innerHTML = tempGbNode;
  colorNode.innerHTML = tempColorNode;
}

// change the currency format to number format
var x = function(price) {
  return price.replace(".", "");
};
// convert css class/id written by BEM to javascript variable name:
function bemToVar(str) {
  return str.replace(/(.+)(__)(.+)/, "$3");
}
// check if the input price is valid

function checkInputPrice(baseprice) {
  return Number.isInteger(baseprice) && baseprice > 999999;
}

// define the array of all the class names of all the radio fields
let classNamesOfFields = [
  "eval__physical",
  "eval__warranty",
  "eval__box",
  "eval__vn",
  "eval__fromshopdunk",
  "eval__willshopdunk"
];

// define the array of all the class names of all the radio fields
let classNamesOfNonCalFields = ["eval__color", "eval__gb"];

// id of dropdown "phone sku"
let idOfFields = ["eval__sku"];

// create a random session ID
function createSessionId(){
  function createRandomChar(){
   return String.fromCharCode(65+Math.floor(Math.random() * 26))   
  }
  function createRandomNum(){
  return Math.floor(Math.random() * 10);  
  }
  let s1 = createRandomChar();
  let s3 = createRandomChar();
  let s5 = createRandomChar();
  let s2 = createRandomChar();
  let s4 = createRandomChar();
  let s6 = createRandomChar();
return s1+s2+s3+s4+s5+s6;
 
}



// add their name and phone number when someone click Receive Value
function buttonReceive() {
  anEvaluation.sessionID = createSessionId();
  anEvaluation.name = document.getElementById("eval__button-name").value;
  anEvaluation.phonenum = document.getElementById(
    "eval__button-phonenum"
  ).value;
  if (
    anEvaluation.name !== "" &&
    anEvaluation.phonenum.length >= 10
  ) {
    // localStorage.setItem("loSto2", JSON.stringify(anEvaluation));
    // go to result page
    // location.href = "../pages/result.html";
    // document.getElementById("eval__page-3").style.display = "block";
    // document.getElementById("eval__page-1").style.display = "none";
    // document.getElementById("eval__page-2").style.display = "none";
    showWhichPage("eval__page-3");
    // runProgressBar();
    document.getElementById("eval__sms").innerHTML = printMessage();
  } else {
    alert("Vui lòng nhập đúng họ tên và số điện thoại");
  }
}
function showWhichPage(pageid){
  listPages = ["eval__page-3","eval__page-1","eval__page-2"];
  listPages.map(page => {
    if (page == pageid){
      document.getElementById(page).style.display = "block";
    } else{
      document.getElementById(page).style.display = "none"; 
    }
    
  }) 
  
}

//back to previous page of dinh gia
function buttonReturn() {
  if (confirm("Quay lại trang định giá sẽ reset lại các lựa chọn")) {
    // location.href = "../index.html";
    // window.localStorage.clear();
    location.reload();
  }
}
// clear all the fields
function buttonReset() {
  if (confirm("Bạn muốn reset lại các lựa chọn?")) {
    location.reload();
    // window.localStorage.clear();
  }
}

// trigger calculation when someone click submit
function buttonEval() {
  // anEvaluation is a JS object, evaluatePrice  will check if the customer fill in all info, if yes, href to the next page
  anEvaluation.evaluatePrice();
  // localStorage.setItem("loSto1", JSON.stringify(anEvaluation));
}

// onclick to the Question Mark (Help section)
[...document.getElementsByClassName("eval__question-mark")].map(eachnode =>
  eachnode.addEventListener("click", function() {
    this.nextElementSibling.style.display = "block";
  })
);

// onclick to the close help windows
[...document.getElementsByClassName("eval__help-close")].map(eachnode => {
  eachnode.addEventListener("click", function() {
    this.parentNode.style.display = "none";
  });
});
// // click outside of the help window also close it
// window.onload = function() {
//   panels.map(
//     eachpanel =>
//       function() {
//         document.onclick = function(e) {
//           if (e.target.id !== eachpanel) {
//             eachpanel.style.display = "none";
//           }
//         };
//       }
//   );
// };

// progress bar mocking up sending messages

function runProgressBar() {
  const message = document.getElementsByClassName("eval__progress-message")[0];

  var bar = document.getElementsByClassName("eval__bar")[0];
  var width = 1;
  var id = setInterval(frame, 150);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
      message.innerText = "Kết quả định giá đã được gửi thành công!";
    } else {
      width++;
      bar.style.width = width + "%";
    }
  }
}

// content of the message

function printMessage() {
  // let loSto1 = JSON.parse(localStorage.loSto1);
  // let loSto2 = JSON.parse(localStorage.loSto2);
  // // roound the numbers so result 10.300.466 can turn into 10.300.000
  let resultRounded = Math.round(anEvaluation.result / 1000) * 1000;
  return ` 
 <p class="eval__title"> ShopDunk cám ơn bạn đã sử  dụng dịch vụ định giá! </p> <br/>
    
 <p> Thông tin điện thoại của bạn: ${anEvaluation.sku}, màu ${
      anEvaluation.color
  }, dung lượng ${anEvaluation.gb.toUpperCase()}, ${
    companyRate.physical[anEvaluation.physical].message
  }, ${companyRate.warranty[anEvaluation.warranty].message}, ${
    companyRate.box[anEvaluation.box].message
  }, ${companyRate.vn[anEvaluation.vn].message}, ${
    companyRate.fromshopdunk[anEvaluation.fromshopdunk].message
  }. </p><br/>
    
   <p> Kết quả định giá: <strong>${resultRounded
      .toString()
      .replace(/(?<=\d)(?=(\d\d\d)+(?!\d))/g, ".")} đ </strong></p>


     <p> Mã lượt định giá: <strong>${anEvaluation.sessionID}</strong></p>

       `;
}

// add thousand separator to the input "give base price" while typing
function formatNumber(id) {
  document.getElementById(id).addEventListener("keyup", function(event) {
    if (event.keyCode >= 37 && event.keyCode <= 40) return;
    this.value = this.value
      .replace(/\D/g, "")
      .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  });
}

// not allow number and special chars on Full Name
function formatFullName(id) {
  document.getElementById(id).addEventListener("keyup", function(event) {
    if (event.keyCode >= 37 && event.keyCode <= 40) return;
    this.value = this.value.replace(/[\d!@#$%^&*()-_+=;:"]/g, "");
  });
}

// only allow number on phone number
function formatPhoneNum(id) {
  document.getElementById(id).addEventListener("keyup", function(event) {
    if (event.keyCode >= 37 && event.keyCode <= 40) return;
    this.value = this.value.replace(/\D/g, "");
  });
}

// accordion

const accordion = document.getElementById("eval__accordion");
const panels = [...document.getElementsByClassName("eval__panel")];
const panel = document.getElementsByClassName("eval__panel")[0];

const evalsku = document.getElementById("eval__sku");
let gbNode = document.getElementById("eval__list-gb");
let colorNode = document.getElementById("eval__list-color");

evalsku.addEventListener("change", function() {
  if (evalsku !== "defaultsku") {
    panel.style.display = "block";
    printColorandGb(); // print colors and gb to the screen
  }
});

document.getElementById("eval__page-2").style.display = "none";
document.getElementById("eval__page-3").style.display = "none";
