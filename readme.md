# Mục đích

- Khách cần bán điện thoại cũ (có thể là mua từ ShopDunk hoặc nơi khác), sẽ vào trang dinhgia.shopdunk.com để xác định giá trị máy của họ... Khách nhập đầy đủ thông tin điện thoại và thông tin cá nhân là xem được kết quả định giá luôn.
- Lấy họ tên, sđt, thông tin vê điện thoại của khách, lưu vào database, để CSKH thu thập dữ liệu

Lưu ý: Phần chọn điện thoại ở đầu trang (ví dụ chọn iPhone 6 32GB màu Đen...) chỉ để thu thập thông tin khách, không ảnh hưởng đến quá trình định giá. Giá sản phẩm không tự nhảy ra được từ việc chọn điện thoại, khách vẫn phải tự nhập giá (xem giá trên shopdunk.com rồi tự nhập).

# Output

- Mỗi lượt truy cập trang sẽ tạo 1 instance của class Evaluation, đặt tên object instance này là anEvaluation chứa các key sau:
    transactionid (mã lượt định giá)
    sku (tên điện thoại, ví dụ iPhone 11)
    gb (dung lượng, ví dụ 16GB)
    color (màu sắc)
    baseprice (giá ShopDunk bán mới)
    result (kết quả định giá)
    physical (hình thức máy, vd Loại A)
    warranty (thời hạn bảo hành)
    box (hộp)
    vn (phiên bản việt nam hay quốc tế)
    fromshopdunk (máy này mua ở ShopDunk hay đâu)
    willshopdunk (sẽ tiếp tục mua máy ở ShopDunk hay không)
- Sau các bước, thông tin khách hàng sẽ được bổ sung vào obj anEvaluation
- Các thông tin trong object anEvaluation này cần được lưu vào database

# Mô tả code

## Các function gọi ngay khi load trang

- formatFullName: ngăn ko cho nhập số và kí tự đặc biệt vào input tên
- formatPhoneNum: ngăn không cho nhập gì ngoài số vào input sđt
- formatNumber: giá tiền máy điện thoại, viết theo kiểu dùng dấu chấm để tách 3 chữ số

## Các màn hình

- Màn hình đầu:
    - khi khách nhấn nút Định Giá, chạy function buttonEval() (sự kiện ở onclick trong trực tiếp button Định giá, function khai báo ở script.js). Bên trong function buttonEval có function evaluatePrice() 
           - trong function evaluatePrice():  kiểm tra khách đã nhập thông tin điện thoại đầy đủ chưa, nếu đủ rồi thì sang màn hình thứ 2
           - ngoài ra function buttonEval sẽ lưu thông tin về điện thoại của khách vào object  anEvaluation
    - Khi khách nhấn reset thì chạy function buttonReset() (gán onclick ở button luôn, function thì khai báo ở script.js) để tải lại trang
- Màn hình nhập tên sđt:
    - khi khách bấm nút  Nhận Kết Quả thì kích hoạt function buttonReceive(), func này viết ở script.js, func này kiểm tra xem tên và sđt của khách mà đạt yêu cầu thì nhảy sang trang kết quả, đồng thời lưu tên và sđt của khách vào object anEvaluation, kích hoạt function printMessage để đẩy vào DOM node
    - Khi khách bấm quay lại thì kích hoạt function buttonReturn để tải lại trang
    
- Màn hình kết quả định giá: 
    - dùng printMessage() để in kết quả ra DOM node


# update 22/10/2019

đã comment out tính năng fake cái  progressbar "đang gửi tin nhắn" ở trang cuối, và comment out phần gọi function runProgressBar();